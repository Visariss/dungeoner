﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDAttributes : MonoBehaviour {

    public Player player;

    private Text text;
    
    private void Start () {
        text = GetComponent<Text>();        
        updateValues();
    }

    public void updateValues () {
        text.text = string.Format("{0}\n{1}/{2}\n{3}/{4}\n\n{5}\n{6}\n{7}\n\n{8}\n{9}\n\n{10}/{11}\n\n{12}",
                                  player.level.ToString(),
                                  player.currentHealth.ToString(), player.totalMaxHealth.ToString(),
                                  player.currentMana.ToString(), player.totalMaxMana.ToString(),
                                  player.totalAttack.ToString(),
                                  player.totalDefense.ToString(),
                                  player.totalKnowledge.ToString(),
                                  player.totalStrength.ToString(),
                                  player.totalIntelligence.ToString(),
                                  player.getCurrentExp().ToString(), player.getExpToLevelUp().ToString(),
                                  player.getLearningPoints().ToString());
    }
}
