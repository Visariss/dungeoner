﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HealthBar : MonoBehaviour {

    private GameObject border;
    private GameObject health;
    private GameObject owner;
    private Character characterComponent;

    private float borderWidth = 1f;
    private float borderHeight = 0.1f;

    private float healthWidth = 0.95f;
    private float healthHeight = 0.05f;
    
	private void Start () {
        owner = transform.parent.gameObject;
        characterComponent = owner.GetComponent<Character>();
        for (int i = 0; i < transform.childCount; i++) {
            Transform child = transform.GetChild(i);
            if (child.name == "Border") {
                border = child.gameObject;
            }
            if (child.name == "Health") {
                health = child.gameObject;
            }
        }
        UpdateHealthBar(1f);
    }

    private void Update() {
        UpdateHealthBar((float)characterComponent.currentHealth/(float)characterComponent.totalMaxHealth);
    }

    public void UpdateHealthBar(float percentage) {
        health.GetComponent<RectTransform>().sizeDelta = new Vector2(healthWidth * percentage, healthHeight);
        if (percentage > 0.5f) {
            float red = (1f - percentage) / 0.5f;
            health.GetComponent<Image>().color = new Color(red, 1f, 0);
        } else if (percentage < 0.5f) {
            float green = percentage / 0.5f;
            health.GetComponent<Image>().color = new Color(1f, green, 0);
        } else {
            health.GetComponent<Image>().color = new Color(1f, 1f, 0);
        }
    }
}
