﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System;

public class InventorySlot : MonoBehaviour, IPointerClickHandler, IPointerEnterHandler, IPointerExitHandler, IDropHandler {

    public enum type {
        inventorySlot,
        weaponSlot,
        shieldSlot,
        helmetSlot,
        chestSlot,
        trousersSlot,
        bootsSlot,
        schemeSlot,
        materialSlot
    }

    public GameObject templateItemWindow;
    public type slotType;

    private GameObject content;
    private Image image;

    private void Awake () {
	    content = null;
        image = GetComponent<Image>();
    }

    private bool itemFilter() {
        var draggedItem = Item.draggedItem.GetComponent<Item>();
        switch (slotType) {
            case type.inventorySlot:
                return true;
            case type.weaponSlot:
                Debug.Log(Item.draggedItem.GetComponent<Item>().itemCategory);
                if (draggedItem.itemCategory == Item.category.weapon) return true;
                break;
            case type.shieldSlot:
                if (draggedItem.itemCategory == Item.category.shield) return true;
                break;
            case type.helmetSlot:
                if (draggedItem.itemCategory == Item.category.helmet) return true;
                break;
            case type.chestSlot:
                if (draggedItem.itemCategory == Item.category.chest) return true;
                break;
            case type.trousersSlot:
                if (draggedItem.itemCategory == Item.category.trousers) return true;
                break;
            case type.bootsSlot:
                if (draggedItem.itemCategory == Item.category.boots) return true;
                break;
            case type.schemeSlot:
                if (draggedItem.itemCategory == Item.category.scheme) return true;
                break;
            case type.materialSlot:

                var player = Player.player.GetComponent<Player>();
                var inventory = player.inventory.GetComponent<HUDInventory>();
                var usedMaterial = inventory.getTypeOfUsedMaterial();
                if (draggedItem.itemType == usedMaterial) return true;

                if (usedMaterial == Item.type.none) {
                    var schemeSlot = inventory.getSchemeFromSlot().GetComponent<InventorySlot>();
                    var contentScheme = schemeSlot.getContent();

                    if (contentScheme) {
                        var scheme = contentScheme.GetComponent<Scheme>();
                        if (draggedItem.itemCategory == scheme.requiredCategoryOfMaterial) {
                            return true;
                        }
                    }
                }
                
                break;
        }
        return false;
    }

    public void OnPointerClick(PointerEventData eventData) {
        if (content) {
            if (!HUDControl.itemMenu) {
                HUDControl.inventoryState = HUDControl.state.menu;
                if (HUDControl.itemWindow) Destroy(HUDControl.itemWindow);
                HUDControl.itemMenu = Instantiate<GameObject>(content.GetComponent<Item>().itemMenu);
                HUDControl.itemMenu.transform.position = gameObject.transform.position;
                HUDControl.itemMenu.transform.SetParent(transform.parent.parent);
                HUDControl.itemMenu.transform.SetAsLastSibling();

                HUDControl.itemMenu.GetComponent<ItemMenu>().setSlot(gameObject);
            }
        }
    }

    public void OnPointerEnter(PointerEventData eventData) {
        if (HUDControl.inventoryState == HUDControl.state.idle) {
            if (!HUDControl.itemWindow) {
                HUDControl.itemWindow = Instantiate<GameObject>(templateItemWindow);
                HUDControl.itemWindow.transform.position = gameObject.transform.position;
                HUDControl.itemWindow.transform.SetParent(transform.parent.parent);
                HUDControl.itemWindow.transform.SetAsLastSibling();
                if (content) {
                    HUDControl.itemWindow.GetComponentInChildren<Text>().text = content.GetComponent<Item>().getText();
                } else {
                    HUDControl.itemWindow.GetComponentInChildren<Text>().text = "Empty Slot";
                }
            }
        }
    }

    public void OnPointerExit(PointerEventData eventData) {
        if (HUDControl.itemWindow) Destroy(HUDControl.itemWindow);
    }

    public void OnDrop(PointerEventData eventData) {
        if (!itemFilter()) return;

        Item.dropped = true;
        Item.draggedItem.transform.SetParent(transform);

        Item.draggedFromSlot.setContent(content);

        setContent(Item.draggedItem);

        Player.player.GetComponent<Player>().inventory.updateStatsBonusProvidedByEquipment();
    }

    public GameObject getContent() {
        return content;
    }

    public void setContent(GameObject item) {
        content = item;
        if (content) {
            content.transform.position = transform.position;
            content.transform.SetParent(transform);
        }
    }
}
