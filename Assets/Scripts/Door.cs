﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public enum type {
        wooden
    }

    public type doorType;

    public void OnMouseUp() {
        Player.player.GetComponent<Player>().open(this);
    }
}
