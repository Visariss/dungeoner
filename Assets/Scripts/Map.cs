﻿using UnityEngine;
using UnityEditor;
using System;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[ExecuteInEditMode]
public class Map : MonoBehaviour {
    public int rows;
    public int columns;

    public GameObject brush;

    public int layer = 0;

    public static Transform map;

    public static int mapWidth;
    public static int mapHeight;
    public static int tileWidth  = 1;
    public static int tileHeight = 1;

    public static Vector3 tileCenter;
    public static Vector3 mapPos;
    public static Vector3 mousePos;
    public static Vector3 markerPos;

    private void Start() {
        if (!Map.map) {
            Map.map = transform;
        }
    }

    public Map() {
        this.rows          = 4;
        this.columns       = 4;
        Map.tileCenter     = new Vector3(Map.tileWidth / 2f,
                                         Map.tileHeight / 2f,
                                         0);
        Map.mapWidth = this.columns * Map.tileWidth;
        Map.mapHeight = this.rows * Map.tileHeight;
    }

    private Vector3 getTileFromMousePos() {
        var tile = new Vector3((mousePos.x - transform.position.x) / Map.tileWidth,
                               (mousePos.y - transform.position.y) / Map.tileHeight,
                               this.transform.position.z);
        var row = (int)tile.y;
        var col = (int)tile.x;

        if (row < 0) row = 0;
        if (row > this.rows - 1) row = this.rows - 1;
        if (col < 0) col = 0;
        if (col > this.columns - 1) col = this.columns - 1;

        return new Vector3(col, row, 0);
    }

    public void OnDrawGizmos() {
        //
        // Draw map border
        //=====================================================================

        mapWidth  = this.columns * Map.tileWidth;
        mapHeight = this.rows * Map.tileHeight;

        var LDPos      = this.transform.position;
        var LUPos      = LDPos + new Vector3(0, mapHeight, 0);
        var RDPos      = LDPos + new Vector3(mapWidth, 0, 0);
        var RUPos      = LDPos + new Vector3(mapWidth, mapHeight, 0);

        Gizmos.color = Color.white;
        Gizmos.DrawLine(LDPos, RDPos);
        Gizmos.DrawLine(LDPos, LUPos);
        Gizmos.DrawLine(LUPos, RUPos);
        Gizmos.DrawLine(RDPos, RUPos);
        //=====================================================================
        
        Map.mapPos = transform.position;
        Map.mousePos = Utilities.GUIMousePositionInWorldSpace();
        Map.markerPos = LDPos + getTileFromMousePos();
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(markerPos + Map.tileCenter, new Vector3(1, 1, 0));
        SceneView.RepaintAll();
    }
}
