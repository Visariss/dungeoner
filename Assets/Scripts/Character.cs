﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class Character : Interactable {

    public enum type {
        player,
        skeleton001
    }

    public static float clickDistance;

    public GameObject templateCharacterWindow;
    public GameObject templateCombatText;
    public GameObject templateHealthBar;
    public Character.type characterType;

    protected GameObject healthBar;

    // PRIMARY STATS

    public int level;

    public int characterMaxHealth;
    [HideInInspector] public int learnedMaxHealth;
    [HideInInspector] public int equipmentMaxHealth;
    [HideInInspector] public int totalMaxHealth;

    public int characterMaxMana;
    [HideInInspector] public int learnedMaxMana;
    [HideInInspector] public int equipmentMaxMana;
    [HideInInspector] public int totalMaxMana;

    public int characterStrength;
    [HideInInspector] public int learnedStrength;
    [HideInInspector] public int equipmentStrength;
    [HideInInspector] public int totalStrength;

    public int characterIntelligence;
    [HideInInspector] public int learnedIntelligence;
    [HideInInspector] public int equipmentIntelligence;
    [HideInInspector] public int totalIntelligence;

    // SECONDARY STATS

    public int characterAttack;
    [HideInInspector] public int learnedAttack;
    [HideInInspector] public int equipmentAttack;
    [HideInInspector] public int totalAttack;

    public int characterDefense;
    [HideInInspector] public int learnedDefense;
    [HideInInspector] public int equipmentDefense;
    [HideInInspector] public int totalDefense;

    public int characterKnowledge;
    [HideInInspector] public int learnedKnowledge;
    [HideInInspector] public int equipmentKnowledge;
    [HideInInspector] public int totalKnowledge;
    
    [HideInInspector] public int currentHealth;
    [HideInInspector] public int currentMana;

    // OTHER STATS

    public int initExpToLevelUp;
    public int currentExp;
    public int learningPoints;

    ////////////////////////////

    protected float attackCooldown;
    protected float lastAttackTime;

    protected string text;
    protected Vector3 visionDirection;
    
    public float attackRange;
    public float visionRange;
    public float movementSpeed;

    public int expReward;
    public List<GameObject> rewardsList;
    public List<int> rewardsMinAmount;
    public List<int> rewardsMaxAmount;

    private GameObject characterWindow;

    public void updateCharacterDescription() {
        text = string.Format("HP: {0}/{1}\nAtk: {2}\nDef: {3}",
                             currentHealth.ToString(), totalMaxHealth.ToString(),
                             totalAttack.ToString(),
                             totalDefense.ToString());
    }

    protected void Init() {
        player = Player.player;
        visionDirection = new Vector3(0, 1f, 0);
        attackCooldown = 1.5f;
        lastAttackTime = 0.0f;
    }

    protected void handleCharacterWindow() {
        if (characterWindow) {
            characterWindow.transform.rotation = Quaternion.Euler(0, 0, 0);
        }
    }

    protected void handleHealthBar() {
        if (healthBar == null) {
            healthBar = Instantiate<GameObject>(templateHealthBar);
            healthBar.transform.position = transform.position + new Vector3(0, 0.55f, 0);
            healthBar.transform.SetParent(transform);
        }

        if (healthBar) {
            if (Vector3.Distance(transform.position, player.transform.position) <= Player.torchLightRange / 2
                && IsVisible()
                && IsWounded()) {
                healthBar.SetActive(true);
            } else {
                healthBar.SetActive(false);
            }

            healthBar.transform.rotation = Quaternion.Euler(0, 0, 0);
            healthBar.transform.position = transform.position + new Vector3(0, 0.55f, 0);
        }
    }

    protected bool IsWounded() {
        return (currentHealth < totalMaxHealth) ? true : false;
    }

    protected bool IsPlayerVisible() {
        string [] blockingLayers = {"Wall", "Door"};
        if (Vector3.Distance(transform.position, player.transform.position) < visionRange) {
            foreach (string blockingLayer in blockingLayers) {
                var hit = Physics2D.Linecast(transform.position, player.transform.position, 1 << LayerMask.NameToLayer(blockingLayer));                
                if (hit) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }

    protected bool IsPlayerInAttackRange() {
        if (Vector3.Distance(transform.position, player.transform.position) < attackRange) {
            return true;
        }
        return false;
    }

    protected void FollowMouse() {
        Vector3 newVisionDirection = Utilities.gameMousePositionInWorldSpace() - transform.position;
        float rotate_angle = Vector3.Angle(Vector3.up, newVisionDirection);
        float sign = newVisionDirection.x <= 0 ? 1.0f : -1.0f;
        transform.rotation = Quaternion.Euler(0, 0, rotate_angle * sign);
    }

    protected void FollowPlayer() {
        if (IsPlayerVisible()) {
            Vector3 newVisionDirection = Player.player.transform.position - transform.position;
            float rotate_angle = Vector3.Angle(Vector3.up, newVisionDirection);
            float sign = newVisionDirection.x >= 0 ? 1.0f : -1.0f;
            transform.rotation = Quaternion.Euler(0, 0, -rotate_angle * sign);
            visionDirection = newVisionDirection;
            if (!IsPlayerInAttackRange()) {
                transform.position += movementSpeed * visionDirection;
            }
        }
    }

    void OnMouseOver() {
        if (IsVisible()) {
            updateCharacterDescription();
            if (!characterWindow) {
                characterWindow = Instantiate<GameObject>(templateCharacterWindow);
                characterWindow.transform.position = gameObject.transform.position + new Vector3(0, 0, -10);
                characterWindow.transform.SetParent(transform);
                characterWindow.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
            }
            characterWindow.GetComponentInChildren<Text>().text = text;
        }
    }

    void OnMouseExit() {
        if (characterWindow) {
            Destroy(characterWindow);
        }
    }

    void spawnReward() {
        var index = Random.Range(0, rewardsList.Count);
        GameObject reward = (GameObject)PrefabUtility.InstantiatePrefab(rewardsList[index]);
        reward.transform.SetParent(transform.parent);
        reward.transform.position = transform.position;
        reward.GetComponent<Item>().amount = Random.Range(rewardsMinAmount[index], rewardsMaxAmount[index] + 1);
    }

    public void HealthRecoverFull() {
        currentHealth = totalMaxHealth;
    }

    public void ManaRecoverFull() {
        currentMana = totalMaxMana;
    }

    void UpdatePrimaryStats() {
        learnedMaxHealth = 10 * level;
        learnedMaxMana = 10 * level;

        totalMaxHealth = characterMaxHealth + learnedMaxHealth + equipmentMaxHealth;
        totalMaxMana = characterMaxMana + learnedMaxMana + equipmentMaxMana;
        totalStrength = characterStrength + learnedStrength + equipmentStrength;
        totalIntelligence = characterIntelligence + learnedIntelligence + equipmentIntelligence;
    }

    void UpdateSecondaryStats() {
        learnedAttack = totalStrength;
        learnedDefense = totalStrength;
        learnedKnowledge = totalIntelligence;

        totalAttack = characterAttack + learnedAttack + equipmentAttack;
        totalDefense = characterDefense + learnedDefense + equipmentDefense;
        totalKnowledge = characterKnowledge + learnedKnowledge + equipmentDefense;
    }

    public void UpdateStats() {
        UpdatePrimaryStats();
        UpdateSecondaryStats();
    }

    public void attackTarget(Character targetControl) {
        if (clickDistance < attackRange) {
            Player playerControl = GetComponent<Player>();
            Color damageColor = new Color(1f, 0f, 0f, 1f);
            if (this is Player) {
                damageColor = new Color(1f, 1f, 1f, 1f);
            }

            int damage, targetHP;

            damage = totalAttack - targetControl.totalDefense;
            targetHP = targetControl.currentHealth;
            if (damage > 0) {
                Debug.Log("Player is attacking: " + damage + " damage!");
                targetControl.currentHealth = targetHP - damage;
                targetControl.displayFloatingText(damage.ToString(), damageColor);
            } else {
                Debug.Log("Player is attacking: 0 damage!");
                targetControl.displayFloatingText("0", damageColor);
            }

            targetHP = targetControl.currentHealth;
            Debug.Log("Target HP: " + targetHP);
            if (targetHP <= 0) {
                if (this is Player) {
                    playerControl.gainExp(targetControl.expReward);
                }
                targetControl.killCharacter();
            }
        }
    }

    public void killCharacter() {
        spawnReward();
        Destroy(gameObject);
    }

    public void displayFloatingText(string text, Color color) {
        var floatingText = Instantiate<GameObject>(templateCombatText);
        floatingText.transform.position = transform.position + new Vector3(0, 0.5f, 0);
        floatingText.transform.SetParent(FloatingTexts.getInstanceTransform());
        floatingText.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
        floatingText.GetComponentInChildren<Text>().text = text;
        floatingText.GetComponentInChildren<Text>().color = color;
        FloatingTexts.floatingTexts.Add(floatingText);
        foreach (GameObject ct in FloatingTexts.floatingTexts) {
            ct.transform.position = new Vector3(ct.transform.position.x, ct.transform.position.y + 0.1f, ct.transform.position.z);
        }
    }
}
