﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dictionaries : MonoBehaviour {

    // items prefabs
    public GameObject goldenCoins;
    public GameObject bronzeKey;

    public GameObject swordScheme001;
    public GameObject shieldScheme001;
    public GameObject helmetScheme001;
    public GameObject chestScheme001;
    public GameObject trousersScheme001;
    public GameObject bootsScheme001;

    public GameObject ironIngot;
    public GameObject swordIron001;
    public GameObject shieldIron001;
    public GameObject helmetIron001;
    public GameObject chestIron001;
    public GameObject trousersIron001;
    public GameObject bootsIron001;

    // characters prefabs
    public GameObject player;
    public GameObject skeleton001;

    public static Dictionary<Item.type, GameObject> itemsDictionary;
    public static Dictionary<Character.type, GameObject> enemiesDictionary;

    private void Awake() {
        Dictionaries.itemsDictionary = new Dictionary<Item.type, GameObject> {
            { Item.type.goldenCoins, goldenCoins },
            { Item.type.bronzeKey, bronzeKey },

            { Item.type.swordScheme001, swordScheme001 },
            { Item.type.shieldScheme001, shieldScheme001 },
            { Item.type.helmetScheme001, helmetScheme001 },
            { Item.type.chestScheme001, chestScheme001 },
            { Item.type.trousersScheme001, trousersScheme001 },
            { Item.type.bootsScheme001, bootsScheme001 },

            { Item.type.ironIngot, ironIngot },
            { Item.type.ironSword, swordIron001 },
            { Item.type.ironShield, shieldIron001 },
            { Item.type.ironHelmet, helmetIron001 },
            { Item.type.ironChest, chestIron001 },
            { Item.type.ironTrousers, trousersIron001 },
            { Item.type.ironBoots, bootsIron001 },
        };

        Dictionaries.enemiesDictionary = new Dictionary<Character.type, GameObject> {
            { Character.type.player, player },
            { Character.type.skeleton001, skeleton001 }
        };
    }
}
