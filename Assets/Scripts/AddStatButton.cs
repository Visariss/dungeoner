﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class AddStatButton : MonoBehaviour {

    public Player player;

    private Button button;

    // Use this for initialization
    private void Start () {
        button = GetComponent<Button>();
        if (player.getLearningPoints() > 0) {
            button.interactable = true;
        } else {
            button.interactable = false;
        }
	}

    // Update is called once per frame
    private void Update () {
        if (player.getLearningPoints() > 0) {
            button.interactable = true;
        } else {
            button.interactable = false;
        }
    }
}
