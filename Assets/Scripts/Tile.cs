﻿using UnityEngine;
using System.Collections;

public abstract class Tile {
    public enum type {
        empty,
        floor,
        wall,
        woodenDoor,
        upstairs,
        downstairs,

        woodenKey,

        goldenCoins,

        ironIngot,

        skeleton001
    }
}
