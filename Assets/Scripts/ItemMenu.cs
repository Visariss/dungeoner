﻿using UnityEngine;
using System.Collections;

public class ItemMenu : MonoBehaviour {
    private GameObject slot;

    public void removeItemButton() {
        Destroy(slot.GetComponent<InventorySlot>().getContent());
        slot.GetComponent<InventorySlot>().setContent(null);
        HUDControl.closeItemMenu();
    }

    public void setSlot(GameObject value) {
        slot = value;
    }
    public GameObject getSlot() {
        return slot;
    }
}
