﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;
using System.Collections;

public class HUDInventory : MonoBehaviour {
    public GameObject slot;
    public Player player;
    public GameObject forgeButton;

    private GameObject [,] inventory;

    private GameObject weaponSlot;
    private GameObject shieldSlot;
    private GameObject helmetSlot;
    private GameObject chestSlot;
    private GameObject trousersSlot;
    private GameObject bootsSlot;

    private GameObject schemeSlot;
    private GameObject [,] materials;

    private void Awake () {
        createInventoryTab();
        createEquipmentTab();
        createCraftingTab();
    }

    private void createInventoryTab() {
        inventory = new GameObject[6, 7];
        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 7; ++j) {
                inventory[i, j] = (GameObject)PrefabUtility.InstantiatePrefab(slot);
                inventory[i, j].name = string.Format("Slot_{0}_{1}", i, j);
                inventory[i, j].transform.position = transform.position + new Vector3(-350 + i * 60, 180 - j * 60, 0);
                inventory[i, j].transform.SetParent(transform);
                inventory[i, j].GetComponent<InventorySlot>().slotType = InventorySlot.type.inventorySlot;
            }
        }
    }

    private void createEquipmentTab() {
        // create weapon slot
        weaponSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        weaponSlot.name = "WeaponSlot";
        weaponSlot.transform.position = transform.position + new Vector3(40, 60, 0);
        weaponSlot.transform.SetParent(transform);
        weaponSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.weaponSlot;

        // create shield slot
        shieldSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        shieldSlot.name = "ShieldSlot";
        shieldSlot.transform.position = transform.position + new Vector3(150, 60, 0);
        shieldSlot.transform.SetParent(transform);
        shieldSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.shieldSlot;

        // create helmet slot
        helmetSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        helmetSlot.name = "HelmetSlot";
        helmetSlot.transform.position = transform.position + new Vector3(95, 150, 0);
        helmetSlot.transform.SetParent(transform);
        helmetSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.helmetSlot;

        // create chest slot
        chestSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        chestSlot.name = "ChestSlot";
        chestSlot.transform.position = transform.position + new Vector3(95, 90, 0);
        chestSlot.transform.SetParent(transform);
        chestSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.chestSlot;

        // create trousers slot
        trousersSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        trousersSlot.name = "TrousersSlot";
        trousersSlot.transform.position = transform.position + new Vector3(95, 30, 0);
        trousersSlot.transform.SetParent(transform);
        trousersSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.trousersSlot;

        // create boots slot
        bootsSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        bootsSlot.name = "BootsSlot";
        bootsSlot.transform.position = transform.position + new Vector3(95, -30, 0);
        bootsSlot.transform.SetParent(transform);
        bootsSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.bootsSlot;
    }

    private void createCraftingTab() {

        // create scheme slot
        schemeSlot = (GameObject)PrefabUtility.InstantiatePrefab(slot);
        schemeSlot.name = "SchemeSlot";
        schemeSlot.transform.position = transform.position + new Vector3(295, 120, 0);
        schemeSlot.transform.SetParent(transform);
        schemeSlot.GetComponent<InventorySlot>().slotType = InventorySlot.type.schemeSlot;

        // create material slots
        materials = new GameObject[3, 3];
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                materials[i, j] = (GameObject)PrefabUtility.InstantiatePrefab(slot);
                materials[i, j].name = string.Format("Material_{0}_{1}", i, j);
                materials[i, j].transform.position = transform.position + new Vector3(240 + i * 55, -j * 60, 0);
                materials[i, j].transform.SetParent(transform);
                materials[i, j].GetComponent<InventorySlot>().slotType = InventorySlot.type.materialSlot;
            }
        }
    }

    private int getMaterialAmount() {
        int materialAmount = 0;
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                InventorySlot slot = materials[i, j].GetComponent<InventorySlot>();
                GameObject content = slot.getContent();
                if (content) {
                    materialAmount += content.GetComponent<Item>().amount;
                }
            }
        }
        return materialAmount;
    }

    public Item.type getTypeOfUsedMaterial() {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                InventorySlot slot = materials[i, j].GetComponent<InventorySlot>();
                GameObject content = slot.getContent();
                if (content) {
                    return content.GetComponent<Item>().itemType;
                }
            }
        }
        return Item.type.none;
    }

    public GameObject getSchemeFromSlot() {
        return schemeSlot;
    }

    private void useMaterials(int requiredMaterials) {
        for (int i = 0; i < 3; ++i) {
            for (int j = 0; j < 3; ++j) {
                InventorySlot slot = materials[i, j].GetComponent<InventorySlot>();
                GameObject content = slot.getContent();
                if (content) {
                    var material = content.GetComponent<Item>();
                    if (requiredMaterials > material.amount) {
                        requiredMaterials -= material.amount;
                        Destroy(content);
                    } else {
                        material.amount -= requiredMaterials;
                        material.updateItemDescription();
                        requiredMaterials = 0;
                        if (material.amount == 0) Destroy(content);

                    }
                }
            }
        }
    }

    private void craftItem(Item.type schemeType, Item.type materialType, GameObject content) {
        int schemeIndex = 0;
        int materialIndex = 0;

        // find scheme in craft matrix
        for (int i = 1; i < Item.craftMatrix.GetLength(0); ++i) {
            if (Item.craftMatrix[i, 0] == schemeType) {
                schemeIndex = i;
                break;
            }
        }

        // find material in craft matrix
        for (int i = 1; i < Item.craftMatrix.GetLength(1); ++i) {
            if (Item.craftMatrix[0, i] == materialType) {
                materialIndex = i;
                break;
            }
        }

        // replace scheme by crafted item
        Destroy(content);
        Item.type craftedItemType = Item.craftMatrix[schemeIndex, materialIndex];
        GameObject craftedItemObject = Instantiate<GameObject>(Dictionaries.itemsDictionary[craftedItemType].GetComponent<Item>().UIVersionOfItem);
        Item craftedItem = craftedItemObject.GetComponent<Item>();
        craftedItem.amount = 1;
        craftedItem.inUI = true;
        if (craftedItem is Equipment) {
            ((Equipment)craftedItem).computeStats(Dictionaries.itemsDictionary[schemeType], Dictionaries.itemsDictionary[materialType]);
        }
        schemeSlot.GetComponent<InventorySlot>().setContent(craftedItemObject);
    }

    public void checkCraftingRequirements() {
        GameObject content = schemeSlot.GetComponent<InventorySlot>().getContent();
        if (content) {
            int requiredMaterials = content.GetComponent<Scheme>().requiredAmountOfMaterial;
            int providedMaterials = getMaterialAmount();
            Item.type materialType = getTypeOfUsedMaterial();
            Item.type schemeType = content.GetComponent<Item>().itemType;
            if (requiredMaterials <= providedMaterials) {
                useMaterials(requiredMaterials);
                craftItem(schemeType, materialType, content);
            } else {
                Debug.Log("You need " + (requiredMaterials - providedMaterials).ToString() + " more materials!");
            }
        } else {
            Debug.Log("Scheme slot is empty!");
        }
    }

    public void updateStatsBonusProvidedByEquipment() {
        Player playerControl = Player.player.GetComponent<Player>();
        GameObject content;

        //weaponSlot
        //shieldSlot
        //helmetSlot
        content = chestSlot.GetComponent<InventorySlot>().getContent();
        if (content) {
            playerControl.equipmentDefense += (int)content.GetComponent<Chest>().getDefense();
        }
        //trousersSlot
        //bootsSlot

        playerControl.UpdateStats();
    }

    public bool removeItem(Item.type itemType, int amount) {
        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 7; ++j) {
                InventorySlot inventorySlot = inventory[i, j].GetComponent<InventorySlot>();
                if (inventorySlot.getContent()) {
                    var itemComponentInSlot = inventorySlot.getContent().GetComponent<Item>();
                    if (itemComponentInSlot.itemType == itemType) {
                        if (itemComponentInSlot.amount >= amount) {
                            itemComponentInSlot.amount -= amount;
                            if (itemComponentInSlot.amount > 0) {
                                itemComponentInSlot.updateItemDescription();
                            } else {
                                Destroy(inventorySlot.getContent());
                            }
                            return true;
                        }
                    }
                }
            }
        }
        return false;
    }

    public bool addItem(GameObject item) {
        var itemComponent = item.GetComponent<Item>();
        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 7; ++j) {
                InventorySlot inventorySlot = inventory[i,j].GetComponent<InventorySlot>();
                GameObject UIItem = inventorySlot.getContent();

                if (UIItem) {
                    var itemComponentInSlot = UIItem.GetComponent<Item>();
                    if (itemComponentInSlot.itemType == itemComponent.itemType) {
                        var availableAmountInStack = itemComponentInSlot.maxStackAmount - itemComponentInSlot.amount;
                        if (availableAmountInStack != 0) {
                            if (itemComponent.amount <= availableAmountInStack) {
                                itemComponentInSlot.amount += itemComponent.amount;
                                itemComponentInSlot.updateItemDescription();
                                return true;
                            } else {
                                itemComponentInSlot.amount += availableAmountInStack;
                                itemComponent.amount -= availableAmountInStack;
                                itemComponentInSlot.updateItemDescription();
                            }
                        }
                    }
                }                
            }
        }
        for (int i = 0; i < 6; ++i) {
            for (int j = 0; j < 7; ++j) {
                InventorySlot inventorySlot = inventory[i, j].GetComponent<InventorySlot>();
                GameObject UIItem = inventorySlot.getContent();

                if (!UIItem) {
                    UIItem = Instantiate<GameObject>(item.GetComponent<Item>().UIVersionOfItem);
                    UIItem.GetComponent<Item>().amount = 0;
                    UIItem.GetComponent<Item>().inUI = true;
                    inventorySlot.setContent(UIItem);
                    var itemComponentInSlot = UIItem.GetComponent<Item>();

                    if (itemComponent.amount <= itemComponentInSlot.maxStackAmount) {
                        itemComponentInSlot.amount = itemComponent.amount;
                        itemComponentInSlot.updateItemDescription();
                        return true;
                    } else {
                        itemComponentInSlot.amount = itemComponentInSlot.maxStackAmount;
                        itemComponent.amount -= itemComponentInSlot.maxStackAmount;
                        itemComponentInSlot.updateItemDescription();
                    }
                }
            }
        }
        return false;
    }
}
