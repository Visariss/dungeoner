﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowCaster2D : MonoBehaviour {
    
    public static float lightRange;
    public static float shadowRange;

    public Vector3 vertexNWOffset;
    public Vector3 vertexNEOffset;
    public Vector3 vertexSEOffset;
    public Vector3 vertexSWOffset;

    private Vector3 casterVertexNW;
    private Vector3 casterVertexNE;
    private Vector3 casterVertexSE;
    private Vector3 casterVertexSW;

    private static Vector3 lightSource;
    private static float angleBetweenRays;
    private static GameObject player;
    private static List<Vector3> shadowVertices;
    private static List<int> shadowTriangles;

    private MeshFilter meshFilter;
    private MeshRenderer meshRenderer;
    private Mesh shadowMesh;

    private GameObject shadow;
    private BoxCollider2D allLightColliders;

    public UnityEngine.Material lightMaterial;

    private void Start() {
        SetupCasterOffsetVertices();
        CreateShadow();
    }

    private void Awake() {
        player = Player.player;

        shadowRange = 100f;
    }

    private void Update() {
        lightSource = Player.player.transform.position;

        if (Vector3.Distance(transform.position, Player.player.transform.position) <= Player.torchLightRange) {
            SetupShadow();
            CastShadow();
            shadow.SetActive(true);
        } else {
            shadow.SetActive(false);
        }
    }

    private void CreateShadow() {
        shadow = new GameObject();
        shadow.name = "Shadow";
        shadow.transform.SetParent(transform, false);

        meshFilter = (MeshFilter)shadow.AddComponent(typeof(MeshFilter));                // Add a Mesh Filter component to the light game object so it can take on a form
        meshRenderer = shadow.AddComponent(typeof(MeshRenderer)) as MeshRenderer;      // Add a Mesh Renderer component to the light game object so the form can become visible
        meshRenderer.sortingLayerName = "Shadow";
        meshRenderer.sortingOrder = 10;
        meshRenderer.sharedMaterial = lightMaterial;

        shadowMesh = new Mesh();
        meshFilter.mesh = shadowMesh;

        shadowMesh.MarkDynamic();
        shadowMesh.Clear();

        shadow.transform.Rotate(0.0f, 0.0f, -shadow.transform.parent.rotation.eulerAngles.z);
    }

    private void CastShadow() {
        shadowMesh.triangles = null;
        shadowMesh.vertices = null;
        var vertices = new Vector3[shadowVertices.Count];
        for (int i = 0; i < shadowVertices.Count; i++) {
            vertices[i] = new Vector3(shadowVertices[i].x, shadowVertices[i].y, 0) - shadow.transform.position;
        }
        shadowMesh.vertices = vertices;

        shadowMesh.colors = new Color[shadowMesh.vertices.Length];

        shadowMesh.uv = new Vector2[shadowMesh.vertices.Length];

        var triangles = new int[shadowTriangles.Count];
        for (int i = 0; i < shadowTriangles.Count; i+=3) {
            triangles[i] = shadowTriangles[i];
            triangles[i+1] = shadowTriangles[i+1];
            triangles[i+2] = shadowTriangles[i+2];
        }

        shadowMesh.triangles = triangles;
    }

    private void SetupShadow() {
        SetupCasterVertices();

        var westEdgeX = casterVertexNW.x;
        var eastEdgeX = casterVertexNE.x;
        var northEdgeY = casterVertexNW.y;
        var southEdgeY = casterVertexSW.y;

        var casterMarginalVertices = new Vector3[2];
        var shadowMarginalVertices = new Vector3[2];
        var corner = false;
        var cornerVertex = new Vector3();

        if (lightSource.x < westEdgeX) {
            if (lightSource.y < southEdgeY) {
                casterMarginalVertices[0] = casterVertexNW;
                casterMarginalVertices[1] = casterVertexSE;
                corner = true;
                cornerVertex = casterVertexSW;
            } else if (lightSource.y >= southEdgeY && lightSource.y <= northEdgeY) {
                casterMarginalVertices[0] = casterVertexNW;
                casterMarginalVertices[1] = casterVertexSW;
            } else {
                casterMarginalVertices[0] = casterVertexNE;
                casterMarginalVertices[1] = casterVertexSW;
                corner = true;
                cornerVertex = casterVertexNW;
            }
        }
        else if (lightSource.x >= westEdgeX && lightSource.x <= eastEdgeX) {
            if (lightSource.y < southEdgeY) {
                casterMarginalVertices[0] = casterVertexSW;
                casterMarginalVertices[1] = casterVertexSE;
            } else {
                casterMarginalVertices[0] = casterVertexNE;
                casterMarginalVertices[1] = casterVertexNW;
            }
        }
        else {
            if (lightSource.y < southEdgeY) {
                casterMarginalVertices[0] = casterVertexSW;
                casterMarginalVertices[1] = casterVertexNE;
                corner = true;
                cornerVertex = casterVertexSE;
            } else if (lightSource.y >= southEdgeY && lightSource.y <= northEdgeY) {
                casterMarginalVertices[0] = casterVertexSE;
                casterMarginalVertices[1] = casterVertexNE;
            } else {
                casterMarginalVertices[0] = casterVertexSE;
                casterMarginalVertices[1] = casterVertexNW;
                corner = true;
                cornerVertex = casterVertexNE;
            }
        }
        shadowMarginalVertices[0] = lightSource + Vector3.Normalize(casterMarginalVertices[0] - lightSource) * shadowRange;
        shadowMarginalVertices[1] = lightSource + Vector3.Normalize(casterMarginalVertices[1] - lightSource) * shadowRange;

        shadowVertices = new List<Vector3>();
        shadowTriangles = new List<int>();

        Debug.DrawLine(casterMarginalVertices[0], shadowMarginalVertices[0]);
        Debug.DrawLine(casterMarginalVertices[1], shadowMarginalVertices[1]);
        shadowVertices.Add(casterMarginalVertices[0]);
        shadowVertices.Add(shadowMarginalVertices[0]);
        shadowVertices.Add(casterMarginalVertices[1]);
        shadowVertices.Add(shadowMarginalVertices[1]);

        int index0 = shadowVertices.IndexOf(casterMarginalVertices[0]);
        int index1 = shadowVertices.IndexOf(shadowMarginalVertices[0]);
        int index2 = shadowVertices.IndexOf(casterMarginalVertices[1]);
        int index3 = shadowVertices.IndexOf(shadowMarginalVertices[1]);

        shadowTriangles.Add(index0);
        shadowTriangles.Add(index1);
        shadowTriangles.Add(index3);

        shadowTriangles.Add(index0);
        shadowTriangles.Add(index3);
        shadowTriangles.Add(index2);

        if (corner) {
            shadowVertices.Add(cornerVertex);
            int indexCorner = shadowVertices.IndexOf(cornerVertex);
            shadowTriangles.Add(indexCorner);
            shadowTriangles.Add(index0);
            shadowTriangles.Add(index2);
        }
    }

    private void SetupCasterVertices() {
        casterVertexNW = new Vector3(transform.position.x + vertexNWOffset.x,
                                     transform.position.y + vertexNWOffset.y,
                                     0);

        casterVertexNE = new Vector3(transform.position.x + vertexNEOffset.x,
                                     transform.position.y + vertexNEOffset.y,
                                     0);

        casterVertexSE = new Vector3(transform.position.x + vertexSEOffset.x,
                                     transform.position.y + vertexSEOffset.y,
                                     0);

        casterVertexSW = new Vector3(transform.position.x + vertexSWOffset.x,
                                     transform.position.y + vertexSWOffset.y,
                                     0);
    }

    private bool IsRotated90() {
        if (Mathf.Approximately(transform.rotation.eulerAngles.z, 90.0f)) {
            return true;
        }
        return false;
    }

    private void SetupCasterOffsetVertices() {
        if (IsRotated90()){            
            var vertexNWOffsetBackup = vertexNWOffset;
            var vertexNEOffsetBackup = vertexNEOffset;
            var vertexSEOffsetBackup = vertexSEOffset;
            var vertexSWOffsetBackup = vertexSWOffset;

            vertexNWOffset = Quaternion.Euler(0.0f, 0.0f, 90.0f) * vertexNEOffsetBackup;
            vertexNEOffset = Quaternion.Euler(0.0f, 0.0f, 90.0f) * vertexSEOffsetBackup;
            vertexSEOffset = Quaternion.Euler(0.0f, 0.0f, 90.0f) * vertexSWOffsetBackup;
            vertexSWOffset = Quaternion.Euler(0.0f, 0.0f, 90.0f) * vertexNWOffsetBackup;
        }
    }
}
