﻿using UnityEngine;
using System.Collections;

public abstract class Scheme : Item {
    public Item.category requiredCategoryOfMaterial;
    public int requiredAmountOfMaterial;
}
