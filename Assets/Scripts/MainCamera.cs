﻿using UnityEngine;
using System.Collections;

public class MainCamera : MonoBehaviour {

    public static GameObject camera;

    public Transform target;

    private int   zoom         = 10;
    private float acceleration = 0.05f;

    // Use this for initialization
    private void Start () {
        MainCamera.camera = gameObject;
	    transform.position = new Vector3(0, 0, -zoom);
	}

    // Update is called once per frame
    private void Update () {
        var speed = target.position - (transform.position + new Vector3(0, 0, zoom + target.position.z));
        speed = new Vector3(speed.x * acceleration, speed.y * acceleration, speed.z);

        transform.position += speed + new Vector3(0, 0, -zoom);
	}
}
