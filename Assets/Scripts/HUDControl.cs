﻿using UnityEngine;
using System.Collections;

public class HUDControl : MonoBehaviour {

    private static HUDInventory inventoryWindow;

    public static state inventoryState = state.idle;

    public static GameObject itemWindow;

    public static GameObject itemMenu;

    public enum state {
        idle,
        dragging,
        menu
    }

    // Use this for initialization
    private void Start () {
        inventoryWindow = GetComponentInChildren<HUDInventory>();

        inventoryWindow.gameObject.SetActive(false);
    }

    private void OnGUI() {
        Event e = Event.current;
        if (e.type == EventType.KeyDown) {
            if (e.keyCode == KeyCode.I) {
                inventoryWindow.gameObject.SetActive(inventoryWindow.gameObject.activeSelf ? false : true);
                if (HUDControl.itemMenu) Destroy(HUDControl.itemMenu);
                if (HUDControl.itemWindow) Destroy(HUDControl.itemWindow);
            }
            if (e.keyCode == KeyCode.Escape) {
                switch (inventoryState) {
                    case state.menu:
                        HUDControl.closeItemMenu();
                        break;
                    case state.dragging:
                        break;
                    default:
                        if (HUDControl.itemMenu) Destroy(HUDControl.itemMenu);
                        if (HUDControl.itemWindow) Destroy(HUDControl.itemWindow);
                        inventoryWindow.gameObject.SetActive(false);
                        break;
                }
            }
        }
    }

    public static void closeItemMenu() {
        if (HUDControl.itemMenu)
            Destroy(HUDControl.itemMenu);
        if (HUDControl.itemWindow)
            Destroy(HUDControl.itemWindow);
        inventoryState = state.idle;
    }
}
