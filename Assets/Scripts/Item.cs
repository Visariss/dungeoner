﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public abstract class Item : Interactable, IBeginDragHandler, IDragHandler, IEndDragHandler {

    // if you add new material or scheme
    // update materials, schemes and crafting matrix below!
    public enum type {
        goldenCoins,
        ironIngot,
        bronzeKey,

        ironSword,
        ironShield,
        ironHelmet,
        ironChest,
        ironTrousers,
        ironBoots,

        swordScheme001,
        shieldScheme001,
        helmetScheme001,
        chestScheme001,
        trousersScheme001,
        bootsScheme001,

        none
    }

    public enum category {
        weapon,
        shield,
        helmet,
        chest,
        trousers,
        boots,
        scheme,
        metalMaterial,
        others,

        none
    }

    // update if you add new scheme
    public static List<type> schemes = new List<type> {
        type.swordScheme001,
        type.shieldScheme001,
        type.helmetScheme001,
        type.chestScheme001,
        type.trousersScheme001,
        type.bootsScheme001,
    };
    
    // update if you add new material
    public static List<type> materials = new List<type> {
        type.ironIngot
    };

    // update if you add new scheme or material
    public static Item.type [,] craftMatrix = new Item.type [,] {
        // materials --- horizontal
        // schemes ----- vertical
        {type.none,                type.ironIngot },
        {type.swordScheme001,      type.ironSword },
        {type.shieldScheme001,     type.ironShield },
        {type.helmetScheme001,     type.ironHelmet },
        {type.chestScheme001,      type.ironChest },
        {type.trousersScheme001,   type.ironTrousers },
        {type.bootsScheme001,      type.ironBoots }
    };

    public GameObject templateItemWindow;
    public GameObject UIVersionOfItem;
    public GameObject itemMenu;
    public bool inUI;
    public int amount;
    public int maxStackAmount;
    public type itemType;
    public category itemCategory;

    public static bool dropped;
    public static GameObject draggedItem;
    public static InventorySlot draggedFromSlot;

    protected string text;
    private GameObject itemWindow;

    private Vector3 lastPosition;
    private Transform lastParent;

    public void updateItemDescription() {
        switch (itemType) {
            case type.goldenCoins:
                text = amount.ToString() + " golden coin(s)";
                break;
            case type.ironIngot:
                text = amount.ToString() + " iron ingot(s)";
                break;
            case type.bronzeKey:
                text = amount.ToString() + " bronze key(s)";
                break;
            case type.ironSword:
                text = "Iron sword";
                break;
            case type.ironShield:
                text = "Iron shield";
                break;
            case type.ironHelmet:
                text = "Iron helmet";
                break;
            case type.ironChest:
                text = "Iron chest";
                break;
            case type.ironTrousers:
                text = "Iron trousers";
                break;
            case type.ironBoots:
                text = "Iron boots";
                break;
            case type.swordScheme001:
                text = "Sword scheme";
                break;
            case type.shieldScheme001:
                text = "Shield scheme";
                break;
            case type.helmetScheme001:
                text = "Helmet scheme";
                break;
            case type.chestScheme001:
                text = "Chest scheme";
                break;
            case type.trousersScheme001:
                text = "Trousers scheme";
                break;
            case type.bootsScheme001:
                text = "Boots scheme";
                break;
        }
    }

    private void OnMouseOver() {
        if (IsVisible()) {
            if (!itemWindow) {
                itemWindow = Instantiate<GameObject>(templateItemWindow);
                itemWindow.transform.position = gameObject.transform.position + new Vector3(0, 0, -10);
                itemWindow.transform.SetParent(transform);
                itemWindow.GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
            }
            itemWindow.GetComponentInChildren<Text>().text = text;
        }        
    }

    private void OnMouseExit() {
        if (itemWindow) {
            Destroy(itemWindow);
        }
    }

    public void OnBeginDrag(PointerEventData eventData) {
        HUDControl.inventoryState = HUDControl.state.dragging;
        Item.dropped = false;
        if (inUI) {
            Item.draggedItem = gameObject;
            Item.draggedFromSlot = transform.parent.GetComponent<InventorySlot>();
            if (HUDControl.itemWindow) Destroy(HUDControl.itemWindow);
            lastParent = transform.parent;
            Item.draggedItem.transform.SetParent(transform.parent.parent);
            Item.draggedItem.transform.SetAsLastSibling();
            Item.draggedItem.GetComponent<CanvasGroup>().blocksRaycasts = false;
        }
    }

    public void OnDrag(PointerEventData eventData) {
        if (inUI) {
            transform.position = eventData.position;
        }
    }

    public void OnEndDrag(PointerEventData eventData) {
        if (inUI) {
            if (!Item.dropped) {
                transform.SetParent(lastParent);
                Item.draggedFromSlot.setContent(Item.draggedItem);
            }
            Item.draggedItem.GetComponent<CanvasGroup>().blocksRaycasts = true;
        }
        HUDControl.inventoryState = HUDControl.state.idle;
    }

    public string getText() {
        return text;
    }

    public void setText(string value) {
        text = value;
    }
}
