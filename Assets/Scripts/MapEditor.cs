﻿using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(Map))]
public class MapEditor : Editor {
    private bool IsMouseOnMap() {
        if (Map.mousePos.x > Map.mapPos.x &&
            Map.mousePos.y > Map.mapPos.y &&
            Map.mousePos.x < Map.mapPos.x + Map.mapWidth &&
            Map.mousePos.y < Map.mapPos.y + Map.mapHeight)
            return true;
        return false;
    }

    private void drawTile() {
        var map = (Map)this.target;

        GameObject tile = (GameObject)PrefabUtility.InstantiatePrefab(map.brush);
        tile.transform.position = Map.markerPos + Map.tileCenter + new Vector3(0, 0, -map.layer * 0.1f);
        tile.transform.SetParent(map.transform);
        tile.name = string.Format("Tile_{0}_{1}_" + map.brush.name, Map.markerPos.x, Map.markerPos.y);
    }

    private void OnSceneGUI() {
        Event e = Event.current;
        if (IsMouseOnMap()) {
            if (e.type == EventType.MouseDown || e.type == EventType.MouseDrag) {
                if (e.button == 0) {
                    this.drawTile();
                    e.Use();
                } else if (e.button == 1) {
                    e.Use();
                }
            }
        }
        Selection.activeTransform = Map.map;
    }
}
