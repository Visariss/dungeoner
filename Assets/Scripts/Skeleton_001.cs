﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Skeleton_001 : Character {

    // Use this for initialization
    private void Start() {
        Init();

        UpdateStats();
        HealthRecoverFull();
        ManaRecoverFull();
    }

    private void FixedUpdate() {
        UpdateStats();
        FollowPlayer();
        handleHealthBar();
        handleCharacterWindow();

        if (Vector3.Distance(transform.position, player.transform.position) <= attackRange) {
            if (Time.fixedTime - lastAttackTime >= attackCooldown) {
                attackTarget(player.GetComponent<Player>());
                lastAttackTime = Time.fixedTime;
            }
        }
    }

    public void OnMouseUp() {
        player.GetComponent<Player>().attackTarget(this);
    }
}
