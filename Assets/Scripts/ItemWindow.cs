﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ItemWindow : MonoBehaviour {
    public void renderInWorld() {
        GetComponent<Canvas>().renderMode = RenderMode.WorldSpace;
    }
    public void renderInHUD() {
        GetComponent<Canvas>().renderMode = RenderMode.ScreenSpaceOverlay;
    }
}
