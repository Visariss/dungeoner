﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public static class Utilities {

    public static Vector3 GUIMousePositionInWorldSpace() {
        Vector3 result;

        var p = new Plane(Vector3.forward, Vector3.zero);
        var ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        var hit = new Vector3();
        float dist;

        if (p.Raycast(ray, out dist)) {
            hit = ray.GetPoint(dist);
        }
        result = hit;

        return result;
    }

    public static Vector3 gameMousePositionInWorldSpace() {
        Vector3 result;

        var p = new Plane(Vector3.forward, Vector3.zero);
        var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        var hit = new Vector3();
        float dist;

        if (p.Raycast(ray, out dist)) {
            hit = ray.GetPoint(dist);
        }
        result = hit;

        return result;
    }

    public static bool isCollisionBetweenPoints(Vector3 point1, Vector3 point2, LayerMask layerMask) {
        var hit = Physics2D.Linecast(point1, point2, layerMask);
        if (hit) {
            return true;
        }
        return false;
    }
}
