﻿using UnityEngine;
using System.Collections;

public abstract class Equipment : Item {
    public Item.type scheme;
    public Item.type material;

    public abstract void computeStats(GameObject scheme, GameObject material);
}
