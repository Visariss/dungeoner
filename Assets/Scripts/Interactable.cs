﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Interactable : MonoBehaviour
{
    public static GameObject player;

    protected bool IsVisible() {
        string [] blockingLayers = {"Wall", "Door"};

        if (Vector3.Distance(transform.position, player.transform.position) < player.GetComponent<Player>().visionRange) {
            foreach (string blockingLayer in blockingLayers) {
                var hit = Physics2D.Linecast(transform.position, player.transform.position, 1 << LayerMask.NameToLayer(blockingLayer));                
                if (hit) {
                    return false;
                }
            }
        } else {
            return false;
        }
        return true;
    }
}
