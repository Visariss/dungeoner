﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FloatingTexts : MonoBehaviour {

    public static List<GameObject> floatingTexts;

    private static FloatingTexts instance;

    private void Start() {
        instance = this;
        floatingTexts = new List<GameObject>();
    }
    
    private void Update () {
        if (floatingTexts.Count != 0) {
            for (int i = floatingTexts.Count - 1; i >= 0; i--) { // for instead foreach, because the list is modified in loop
                floatingTexts[i].transform.position += new Vector3(0, 0.005f, 0);
                Text text = floatingTexts[i].GetComponentInChildren<Text>();
                text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - 0.005f);
                if (text.color.a <= 0f) {
                    Destroy(floatingTexts[i]);
                    floatingTexts.RemoveAt(i); // modifying list
                }
            }
        }
    }

    public static Transform getInstanceTransform() {
        return instance.transform;
    }
}
