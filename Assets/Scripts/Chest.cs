﻿using UnityEngine;
using System.Collections;

public class Chest : Equipment {

    private float defense;

    private void Start() {
        computeStats(Dictionaries.itemsDictionary[scheme], Dictionaries.itemsDictionary[material]);
        updateItemDescription();
    }

    public override void computeStats (GameObject scheme, GameObject material) {
        var baseDefense = scheme.GetComponent<ChestScheme>().baseDefense;
        var modifier = material.GetComponent<Material>().modifier;
        defense = baseDefense * modifier;
    }

    public float getDefense() {
        return defense;
    }
}
