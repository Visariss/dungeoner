﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class Player : Character {
    public static GameObject player;
    public static int torchLightRange = 5;

    public HUDAttributes attributes;
    public HUDInventory  inventory;

    // Use this for initialization
    private void Start () {
        Player.player = gameObject;

        Init();

        setLight();

        UpdateStats();
        HealthRecoverFull();
        ManaRecoverFull();
        attributes.updateValues();
    }

    private void FixedUpdate() {
        UpdateStats();
        attributes.updateValues();
        FollowMouse();
        handleHealthBar();
        handleCharacterWindow();
    }

    private void setLight() {
        transform.GetComponentInChildren<Light>().range = Player.torchLightRange;
    }

    private void OnGUI() {
        if (Input.GetKey(KeyCode.W)) {
            transform.position += new Vector3(0, 0.02f, 0);
        }
        if (Input.GetKey(KeyCode.S)) {
            transform.position += new Vector3(0, -0.02f, 0);
        }
        if (Input.GetKey(KeyCode.A)) {
            transform.position += new Vector3(-0.02f, 0, 0);
        }
        if (Input.GetKey(KeyCode.D)) {
            transform.position += new Vector3(0.02f, 0, 0);
        }
        if (Input.GetKeyDown(KeyCode.Mouse0)) {
            clickDistance = Vector3.Distance(Utilities.gameMousePositionInWorldSpace(), transform.position);
        }
    }

    private void OnTriggerEnter2D(Collider2D collider) {
        if (inventory.addItem(collider.gameObject)) {
            displayFloatingText("+" + collider.gameObject.GetComponent<Item>().getText(), new Color(1f, 1f, 0, 1f));
            Destroy(collider.gameObject);
        } else {
            //inventory is full
            //implement this info in window pop up
        }
    }

    public void open(Door door) {
        if (clickDistance < attackRange) {
            switch (door.doorType) {
                case Door.type.wooden:
                    if (inventory.removeItem(Item.type.bronzeKey, 1)) {
                        Destroy(door.gameObject);
                    }
                    break;
            }
        }
    }

    public void fight(Character enemyControl) {
        if (clickDistance < attackRange) {
            Player playerControl = GetComponent<Player>();

            int damage, enemyHP, playerHP;

            damage = playerControl.totalAttack - enemyControl.totalDefense;
            enemyHP = enemyControl.currentHealth;
            if (damage > 0) {
                Debug.Log("Player is attacking: " + damage + " damage!");
                enemyControl.currentHealth = enemyHP - damage;
                enemyControl.displayFloatingText(damage.ToString(), new Color(1f, 1f, 1f, 1f));
            } else {
                Debug.Log("Player is attacking: 0 damage!");
                enemyControl.displayFloatingText("0", new Color(1f, 1f, 1f, 1f));
            }

            enemyHP = enemyControl.currentHealth;
            if (enemyHP > 0) {
                damage = enemyControl.totalAttack - playerControl.totalDefense;
                playerHP = playerControl.currentHealth;
                if (damage > 0) {
                    playerControl.currentHealth = playerHP - damage;
                    playerControl.displayFloatingText(damage.ToString(), new Color(1f, 0, 0, 1f));
                } else {
                    playerControl.displayFloatingText("0", new Color(1f, 0, 0, 1f));
                }

                playerHP = playerControl.currentHealth;
                if (playerHP <= 0) {
                    Debug.Log("Player is dead!");
                }
            } else {
                playerControl.gainExp(enemyControl.expReward);
                enemyControl.killCharacter();
            }
        }
    }

    public void gainExp(int exp) {
        if (currentExp + exp >= initExpToLevelUp) {
            exp -= initExpToLevelUp - currentExp;
            levelUp();
        }
        currentExp += exp;
        attributes.updateValues();
    }

    public void levelUp() {
        level += 1;
        learningPoints += 1;
        initExpToLevelUp = (level + 1) * 50;
        currentExp = 0;

        UpdateStats();
        HealthRecoverFull();
        ManaRecoverFull();
        attributes.updateValues();
    }

    public void addStrength() {
        learningPoints -= 1;
        learnedStrength += 1;
    }

    public void addIntelligence() {
        learningPoints -= 1;
        learnedIntelligence += 1;
    }

    public int getExpToLevelUp() {
        return initExpToLevelUp;
    }
    public int getCurrentExp() {
        return currentExp;
    }
    public int getLearningPoints() {
        return learningPoints;
    }

    
    public void setExpToLevelUp(int value) {
        initExpToLevelUp = value;
    }
    public void setCurrentExp(int value) {
        currentExp = value;
    }
    public void setLearningPoints(int value) {
        learningPoints = value;
    }
}
