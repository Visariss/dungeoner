﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class LinkLocation : MonoBehaviour {

    public GameObject dungeonPrefab;
    public Tile.type entranceTile;
    
    public GameObject entranceObjectInLinkedLocation;
    public GameObject linkedLocation;
    public GameObject thisLocation;

    private void Start() {
        thisLocation = transform.parent.gameObject;
    }

    private void OnCollisionEnter2D(Collision2D collision) {
        if (collision.transform.name == "Player") {
            if (!linkedLocation) {
                linkedLocation = (GameObject)PrefabUtility.InstantiatePrefab(dungeonPrefab);
                linkedLocation.transform.position = new Vector3(-DungeonGenerator.generatedDungeons*1000, -DungeonGenerator.generatedDungeons*1000, 0);
                entranceObjectInLinkedLocation = linkedLocation.GetComponent<DungeonGenerator>().getEntrance(entranceTile);

                entranceObjectInLinkedLocation.GetComponent<LinkLocation>().linkedLocation = transform.parent.gameObject;
                entranceObjectInLinkedLocation.GetComponent<LinkLocation>().entranceObjectInLinkedLocation = gameObject;
            }
            var ent_pos = entranceObjectInLinkedLocation.transform.position;
            Player.player.transform.position = new Vector3(ent_pos.x + 1, ent_pos.y, Player.player.transform.position.z);
            MainCamera.camera.transform.position = Player.player.transform.position;
        }
    }
}
